from flask import request, jsonify, Response, Blueprint

from .db import tasks as tasks_db
from .tasks import fetch_url

api_v1 = Blueprint('api', 'api', url_prefix='')


@api_v1.route('/tasks/', methods=['GET'])
@api_v1.route('/tasks/<task_id>/', methods=['GET'])
def result(task_id=None):
    if task_id:
        task = tasks_db.get_one(task_id)
        if not task:
            return Response(status=404)
        return jsonify(task)
    else:
        tasks = tasks_db.get_many(limit=10)
        return jsonify(tasks)


@api_v1.route('/tasks/', methods=['POST'])
def send():
    data = request.json
    if not data:
        return jsonify({'headers': 'Use Content-Type header'}), 400
    url = data.get('URL')
    if not url:
        return jsonify({'URL': 'This field is required'}), 400
    task = tasks_db.insert({'url': url, 'status': 'New'})
    if not task:
        return Response(status=500)
    fetch_url.delay(task['id'])
    return jsonify({'task_id': task['id']}), 201
