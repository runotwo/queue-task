import requests
from celery import Celery
from requests.exceptions import RequestException

from config import CELERY_BROKER_URL
from .db import tasks as tasks_db

celery = Celery('tasks_celery', broker=CELERY_BROKER_URL)


@celery.task
def fetch_url(task_id):
    task = tasks_db.get_one(task_id)
    if task:
        tasks_db.update_status(task['id'], 'Pending')
        try:
            response = requests.get(task['url'])
            data = {'response_http_status': response.status_code, 'response_body': response.content.decode('utf-8'),
                    'response_content_length': len(response.content), 'status': 'Completed'}
        except RequestException:
            data = {'status': 'Error'}
        tasks_db.update(task['id'], data)
