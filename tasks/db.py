import pymongo
from bson.objectid import ObjectId
from pymongo.errors import PyMongoError

from config import MONGODB_HOST


class Tasks:
    def __init__(self, db):
        self.tasks = db.tasks

    def insert(self, data):
        try:
            self.tasks.insert_one(data)
        except PyMongoError as e:
            return
        return self.update_id(data)

    def get_one(self, task_id):
        query = {'_id': ObjectId(task_id) if ObjectId.is_valid(task_id) else task_id}
        result = self.tasks.find_one(query)
        result = self.update_id(result)
        return result

    def get_many(self, limit=None, filter=None):
        if filter is None:
            filter = {}
        result = self.tasks.find(filter).sort([('_id', pymongo.DESCENDING)])
        if limit:
            result = result.limit(limit)
        result = list(map(self.update_id, result))
        return result

    def update(self, task_id, data):
        query = {'_id': ObjectId(task_id) if ObjectId.is_valid(task_id) else task_id}
        try:
            result = self.tasks.find_one_and_update(query, {'$set': data}, new=True)
        except PyMongoError as e:
            return
        result = self.update_id(result)
        return result

    def update_status(self, task_id, status):
        return self.update(task_id, {'status': status})

    @staticmethod
    def update_id(task):
        if task:
            task['id'] = str(task['_id'])
            del task['_id']
            return task


connection = pymongo.MongoClient(MONGODB_HOST, 27017)
db = connection.email_service
tasks = Tasks(db)
