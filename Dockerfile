FROM python:3.6
ADD . /usr/task-service
RUN pip3 install -r /usr/task-service/requirements.txt
WORKDIR /usr/task-service/
