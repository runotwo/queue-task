from flask import Flask

from config import SECRET_KEY
from tasks import api_v1


app = Flask(__name__, template_folder='docs/templates')
app.config['SECRET_KEY'] = SECRET_KEY

app.register_blueprint(api_v1)