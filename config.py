import os

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

CSRF_ENABLED = os.environ.get('CSRF_ENABLED', True)

CSRF_SESSION_KEY = os.environ.get('CSRF_SESSION_KEY', "secret")

SECRET_KEY = os.environ.get('SECRET_KEY', "secret")

DEBUG = os.environ.get('DEBUG', True)
MONGODB_HOST = os.environ.get('DB_PORT_27017_TCP_ADDR', '0.0.0.0')
CELERY_BROKER_URL = os.environ.get('CELERY_BROKER_URL', 'amqp://guest:guest@localhost:5672/')

